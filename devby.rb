require_relative 'lib/DoS.rb'

if !(Integer(ARGV[0]) rescue false) || (ARGV.length > 1)
  puts 'Pass one integer parameter!'
  return
elsif ARGV[0].to_i < 1
  puts 'Parameter should be > 0'
  return
end

DoS.atack(ARGV[0].to_i)
