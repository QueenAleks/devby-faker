require_relative 'services/FormFiller.rb'

require 'mechanize'
require 'faker'

class Bot
  attr_accessor :login, :password, :email

  def initialize(domains)
    @login = Faker::Internet.user_name(2..11)+Faker::Number.number(5)
    @password = Faker::Internet.password(8, 16)
    @email = @login + domains[rand(domains.length)]
  end
  
  def register
    checkRegistration(FormFiller.fill(@login, @password, @email)) && Emailer.confirm(@email) ? true : false
  end
  
  def log
    f = File.open("#{Dir.pwd}/logs/accounts.txt", 'a')
    f.puts Time.now.to_s + ' ' + @login + ' ' + @password + ' ' + @email
    f.close
  end
  
  private
  
  def checkRegistration(response)
    response.link_with(href: '/login').nil? ? false : true
  end
end