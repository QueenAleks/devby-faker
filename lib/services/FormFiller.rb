require 'mechanize'

class FormFiller
  def self.fill(login, password, email)
    agent = Mechanize.new
    form = agent.get('https://dev.by/registration').form
    form.field_with(name: 'user[username]').value = login
    form.field_with(name: 'user[email]').value = email
    form.field_with(name: 'user[password]').value = password
    form.field_with(name: 'user[password_confirmation]').value = password
    form.checkbox_with(name: 'user_agreement').check
    agent.submit(form)
  end
end