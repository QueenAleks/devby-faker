require 'net/http'
require 'digest'
require 'json'

class Emailer
  KEY = 'CXlA1ZgAnPmshXUxNKENJLuTMusbp1cJwR2jsnXHTYsHsPfHMX'

  def self.getDomains
    JSON.parse(sendRequest 'domains/')
  end
  
  def self.confirm email
    sleep 10
    redirect URI URI.extract((JSON.parse sendRequest('mail/id/', email))[0]['mail_text_only'])[1] rescue false
  end
  
  private
  
  def self.sendRequest path, email = nil
    uri = URI.parse('https://privatix-temp-mail-v1.p.mashape.com/request/' + path + (email.nil? ? '' : Digest::MD5.hexdigest(email)))
    Net::HTTP.start(uri.host, uri.port,:use_ssl => uri.scheme == 'https') do |http|
      req = Net::HTTP::Get.new uri 
      req.add_field('X-Mashape-Key', KEY)
      req.add_field('Accept', 'application/json')
      return http.request(req).body
    end
  end
  
  def self.redirect(link)
    response = Net::HTTP.get_response(link)
    case response.code.to_i
    when 200
      true
    when 301..303
      redirect URI(response['location'])
    end
  end
end